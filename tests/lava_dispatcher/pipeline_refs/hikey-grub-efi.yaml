- class: LxcAction
  description: download files and deploy using lxc
  level: '1'
  max_retries: 1
  name: lxc-deploy
  pipeline:
  - {class: LxcCreateAction, description: create lxc action, level: '1.1', max_retries: 1,
    name: lxc-create-action, summary: create lxc, timeout: 300}
  - {class: LxcCreateUdevRuleAction, description: create lxc udev rule action, level: '1.2',
    max_retries: 1, name: lxc-create-udev-rule-action, summary: create lxc udev rule,
    timeout: 300}
  - {class: LxcStartAction, description: boot into lxc container, level: '1.3', max_retries: 1,
    name: boot-lxc, summary: attempt to boot, timeout: 300}
  - {class: LxcAptUpdateAction, description: lxc apt update action, level: '1.4',
    max_retries: 1, name: lxc-apt-update, summary: lxc apt update, timeout: 300}
  - {class: LxcAptInstallAction, description: lxc apt install packages action, level: '1.5',
    max_retries: 1, name: lxc-apt-install, summary: lxc apt install, timeout: 300}
  - {class: LxcStopAction, description: stop the lxc container, level: '1.6', max_retries: 1,
    name: lxc-stop, summary: stop lxc, timeout: 300}
  - {class: DeployDeviceEnvironment, description: deploy device environment, level: '1.7',
    max_retries: 1, name: deploy-device-env, summary: deploy device environment, timeout: 300}
  - class: OverlayAction
    description: add lava scripts during deployment for test shell use
    level: '1.8'
    max_retries: 1
    name: lava-overlay
    pipeline:
    - {class: SshAuthorize, description: include public key in overlay and authorize
        root user, level: 1.8.1, max_retries: 1, name: ssh-authorize, summary: add
        public key to authorized_keys, timeout: 300}
    - {class: VlandOverlayAction, description: Populate specific vland scripts for
        tests to lookup vlan data., level: 1.8.2, max_retries: 1, name: lava-vland-overlay,
      summary: Add files detailing vlan configuration., timeout: 300}
    - {class: MultinodeOverlayAction, description: add lava scripts during deployment
        for multinode test shell use, level: 1.8.3, max_retries: 1, name: lava-multinode-overlay,
      summary: overlay the lava multinode scripts, timeout: 300}
    - class: TestDefinitionAction
      description: load test definitions into image
      level: 1.8.4
      max_retries: 1
      name: test-definition
      pipeline:
      - {class: GitRepoAction, description: apply git repository of tests to the test
          image, level: 1.8.4.1, max_retries: 1, name: git-repo-action, summary: clone
          git test repo, timeout: 300}
      - {class: TestOverlayAction, description: overlay test support files onto image,
        level: 1.8.4.2, max_retries: 1, name: test-overlay, summary: applying LAVA
          test overlay, timeout: 300}
      - {class: TestInstallAction, description: overlay dependency installation support
          files onto image, level: 1.8.4.3, max_retries: 1, name: test-install-overlay,
        summary: applying LAVA test install scripts, timeout: 300}
      - {class: TestRunnerAction, description: overlay run script onto image, level: 1.8.4.4,
        max_retries: 1, name: test-runscript-overlay, summary: applying LAVA test
          run script, timeout: 300}
      summary: loading test definitions
      timeout: 300
    - {class: CompressOverlay, description: Create a lava overlay tarball and store
        alongside the job, level: 1.8.5, max_retries: 1, name: compress-overlay, summary: Compress
        the lava overlay files, timeout: 300}
    - {class: PersistentNFSOverlay, description: unpack overlay into persistent NFS,
      level: 1.8.6, max_retries: 1, name: persistent-nfs-overlay, summary: add test
        overlay to NFS, timeout: 300}
    summary: overlay the lava support scripts
    timeout: 300
  - {class: ApplyLxcOverlay, description: apply the overlay to the container by copying,
    level: '1.9', max_retries: 1, name: apply-lxc-overlay, summary: apply overlay
      on the container, timeout: 300}
  summary: lxc deployment
  timeout: 300
- class: BootLxcAction
  description: lxc boot into the system
  level: '2'
  max_retries: 1
  name: lxc-boot
  pipeline:
  - {class: LxcStartAction, description: boot into lxc container, level: '2.1', max_retries: 1,
    name: boot-lxc, summary: attempt to boot, timeout: 300}
  - {class: LxcAddStaticDevices, description: Add devices which are permanently powered
      by the worker to the LXC, level: '2.2', max_retries: 1, name: lxc-add-static,
    summary: Add static devices to the LXC, timeout: 300}
  - {class: ConnectLxc, description: connect to the lxc container, level: '2.3', max_retries: 1,
    name: connect-lxc, summary: run connection command, timeout: 300}
  - {class: ExpectShellSession, description: Wait for a shell, level: '2.4', max_retries: 1,
    name: expect-shell-connection, summary: Expect a shell prompt, timeout: 300}
  - {class: ExportDeviceEnvironment, description: Exports environment variables to
      the device, level: '2.5', max_retries: 1, name: export-device-env, summary: Exports
      environment variables action, timeout: 300}
  summary: lxc boot
  timeout: 300
- class: FastbootAction
  description: download files and deploy using fastboot
  level: '3'
  max_retries: 1
  name: fastboot-deploy
  pipeline:
  - {class: ConnectDevice, description: use the configured command to connect serial
      to the device, level: '3.1', max_retries: 1, name: connect-device, summary: run
      connection command, timeout: 900}
  - class: ResetDevice
    description: reboot or power-cycle the device
    level: '3.2'
    max_retries: 1
    name: reset-device
    pipeline:
    - {class: PDUReboot, description: issue commands to a PDU to power cycle a device,
      level: 3.2.1, max_retries: 1, name: pdu-reboot, summary: hard reboot using PDU,
      timeout: 900}
    summary: reboot the device
    timeout: 900
  - class: DownloaderAction
    description: download with retry
    level: '3.3'
    max_retries: 3
    name: download-retry
    pipeline:
    - {class: HttpDownloadAction, description: use http to download the file, level: 3.3.1,
      max_retries: 1, name: http-download, summary: http download, timeout: 900, url: 'http://images.validation.linaro.org/functional-test-images/hikey/boot.img.xz'}
    summary: download-retry
    timeout: 900
  - class: DownloaderAction
    description: download with retry
    level: '3.4'
    max_retries: 3
    name: download-retry
    pipeline:
    - {class: HttpDownloadAction, description: use http to download the file, level: 3.4.1,
      max_retries: 1, name: http-download, summary: http download, timeout: 900, url: 'http://images.validation.linaro.org/functional-test-images/hikey/ptable-aosp-8g.img'}
    summary: download-retry
    timeout: 900
  - class: DownloaderAction
    description: download with retry
    level: '3.5'
    max_retries: 3
    name: download-retry
    pipeline:
    - {class: HttpDownloadAction, description: use http to download the file, level: 3.5.1,
      max_retries: 1, name: http-download, summary: http download, timeout: 900, url: 'http://images.validation.linaro.org/functional-test-images/hikey/system.img.xz'}
    summary: download-retry
    timeout: 900
  - class: FastbootFlashOrderAction
    description: Determine support for each flash operation
    level: '3.6'
    max_retries: 1
    name: fastboot-flash-order-action
    pipeline:
    - {class: ReadFeedback, description: Check for messages on all other namespaces,
      level: 3.6.1, max_retries: 1, name: read-feedback, summary: Read from other
        namespaces, timeout: 900}
    - {class: WaitDeviceBoardID, description: wait for udev device with board ID,
      level: 3.6.2, max_retries: 1, name: wait-device-boardid, summary: wait for udev
        device with board ID, timeout: 900}
    - {class: FastbootFlashAction, description: Run a specified flash command, level: 3.6.3,
      max_retries: 1, name: fastboot-flash-action, summary: Execute fastboot flash
        command, timeout: 900}
    - {class: WaitDeviceBoardID, description: wait for udev device with board ID,
      level: 3.6.4, max_retries: 1, name: wait-device-boardid, summary: wait for udev
        device with board ID, timeout: 900}
    - {class: FastbootFlashAction, description: Run a specified flash command, level: 3.6.5,
      max_retries: 1, name: fastboot-flash-action, summary: Execute fastboot flash
        command, timeout: 900}
    - {class: WaitDeviceBoardID, description: wait for udev device with board ID,
      level: 3.6.6, max_retries: 1, name: wait-device-boardid, summary: wait for udev
        device with board ID, timeout: 900}
    - {class: FastbootFlashAction, description: Run a specified flash command, level: 3.6.7,
      max_retries: 1, name: fastboot-flash-action, summary: Execute fastboot flash
        command, timeout: 900}
    summary: Handle reset and options for each flash url.
    timeout: 900
  summary: fastboot deployment
  timeout: 900
- class: GrubSequenceAction
  description: grub boot sequence
  level: '4'
  max_retries: 1
  name: grub-sequence-action
  pipeline:
  - {class: WaitFastBootInterrupt, description: Check for prompt and pass the interrupt
      string to exit fastboot., level: '4.1', max_retries: 1, name: wait-fastboot-interrupt,
    summary: watch output and try to interrupt fastboot, timeout: 900}
  - class: AutoLoginAction
    description: automatically login after boot using job parameters and checking
      for messages.
    level: '4.2'
    max_retries: 1
    name: auto-login-action
    pipeline:
    - {class: LoginAction, description: Real login action., level: 4.2.1, max_retries: 1,
      name: login-action, summary: Login after boot., timeout: 900}
    summary: Auto-login after boot with support for kernel messages.
    timeout: 900
  summary: run grub boot using specified sequence of actions
  timeout: 900
- class: TestShellRetry
  description: Retry wrapper for lava-test-shell
  level: '5'
  max_retries: 1
  name: lava-test-retry
  pipeline:
  - {class: TestShellAction, description: Executing lava-test-runner, level: '5.1',
    max_retries: 1, name: lava-test-shell, summary: Lava Test Shell, timeout: 600}
  summary: Retry support for Lava Test Shell
  timeout: 600
- class: FinalizeAction
  description: finish the process and cleanup
  level: '6'
  max_retries: 1
  name: finalize
  pipeline:
  - {class: PowerOff, description: discontinue power to device, level: '6.1', max_retries: 1,
    name: power-off, summary: send power_off command, timeout: 10}
  - {class: ReadFeedback, description: Check for messages on all other namespaces,
    level: '6.2', max_retries: 1, name: read-feedback, summary: Read from other namespaces,
    timeout: 900}
  summary: finalize the job
  timeout: 900
