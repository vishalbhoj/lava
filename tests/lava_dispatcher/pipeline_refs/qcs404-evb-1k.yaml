- class: LxcAction
  description: download files and deploy using lxc
  level: '1'
  max_retries: 1
  name: lxc-deploy
  pipeline:
  - {class: LxcCreateAction, description: create lxc action, level: '1.1', max_retries: 1,
    name: lxc-create-action, summary: create lxc, timeout: 300}
  - {class: LxcCreateUdevRuleAction, description: create lxc udev rule action, level: '1.2',
    max_retries: 1, name: lxc-create-udev-rule-action, summary: create lxc udev rule,
    timeout: 300}
  - {class: LxcStartAction, description: boot into lxc container, level: '1.3', max_retries: 1,
    name: boot-lxc, summary: attempt to boot, timeout: 300}
  - {class: LxcAptUpdateAction, description: lxc apt update action, level: '1.4',
    max_retries: 1, name: lxc-apt-update, summary: lxc apt update, timeout: 300}
  - {class: LxcAptInstallAction, description: lxc apt install packages action, level: '1.5',
    max_retries: 1, name: lxc-apt-install, summary: lxc apt install, timeout: 300}
  - {class: LxcStopAction, description: stop the lxc container, level: '1.6', max_retries: 1,
    name: lxc-stop, summary: stop lxc, timeout: 300}
  - {class: DeployDeviceEnvironment, description: deploy device environment, level: '1.7',
    max_retries: 1, name: deploy-device-env, summary: deploy device environment, timeout: 300}
  - class: OverlayAction
    description: add lava scripts during deployment for test shell use
    level: '1.8'
    max_retries: 1
    name: lava-overlay
    pipeline:
    - {class: SshAuthorize, description: include public key in overlay and authorize
        root user, level: 1.8.1, max_retries: 1, name: ssh-authorize, summary: add
        public key to authorized_keys, timeout: 300}
    - {class: VlandOverlayAction, description: Populate specific vland scripts for
        tests to lookup vlan data., level: 1.8.2, max_retries: 1, name: lava-vland-overlay,
      summary: Add files detailing vlan configuration., timeout: 300}
    - {class: MultinodeOverlayAction, description: add lava scripts during deployment
        for multinode test shell use, level: 1.8.3, max_retries: 1, name: lava-multinode-overlay,
      summary: overlay the lava multinode scripts, timeout: 300}
    - class: TestDefinitionAction
      description: load test definitions into image
      level: 1.8.4
      max_retries: 1
      name: test-definition
      pipeline:
      - {class: InlineRepoAction, description: apply inline test definition to the
          test image, level: 1.8.4.1, max_retries: 1, name: inline-repo-action, summary: extract
          inline test definition, timeout: 300}
      - {class: TestOverlayAction, description: overlay test support files onto image,
        level: 1.8.4.2, max_retries: 1, name: test-overlay, summary: applying LAVA
          test overlay, timeout: 300}
      - {class: TestInstallAction, description: overlay dependency installation support
          files onto image, level: 1.8.4.3, max_retries: 1, name: test-install-overlay,
        summary: applying LAVA test install scripts, timeout: 300}
      - {class: TestRunnerAction, description: overlay run script onto image, level: 1.8.4.4,
        max_retries: 1, name: test-runscript-overlay, summary: applying LAVA test
          run script, timeout: 300}
      summary: loading test definitions
      timeout: 300
    - {class: CompressOverlay, description: Create a lava overlay tarball and store
        alongside the job, level: 1.8.5, max_retries: 1, name: compress-overlay, summary: Compress
        the lava overlay files, timeout: 300}
    - {class: PersistentNFSOverlay, description: unpack overlay into persistent NFS,
      level: 1.8.6, max_retries: 1, name: persistent-nfs-overlay, summary: add test
        overlay to NFS, timeout: 300}
    summary: overlay the lava support scripts
    timeout: 300
  - {class: ApplyLxcOverlay, description: apply the overlay to the container by copying,
    level: '1.9', max_retries: 1, name: apply-lxc-overlay, summary: apply overlay
      on the container, timeout: 300}
  summary: lxc deployment
  timeout: 300
- class: BootLxcAction
  description: lxc boot into the system
  level: '2'
  max_retries: 1
  name: lxc-boot
  pipeline:
  - {class: LxcStartAction, description: boot into lxc container, level: '2.1', max_retries: 1,
    name: boot-lxc, summary: attempt to boot, timeout: 300}
  - {class: LxcAddStaticDevices, description: Add devices which are permanently powered
      by the worker to the LXC, level: '2.2', max_retries: 1, name: lxc-add-static,
    summary: Add static devices to the LXC, timeout: 300}
  - {class: ConnectLxc, description: connect to the lxc container, level: '2.3', max_retries: 1,
    name: connect-lxc, summary: run connection command, timeout: 300}
  - {class: ExpectShellSession, description: Wait for a shell, level: '2.4', max_retries: 1,
    name: expect-shell-connection, summary: Expect a shell prompt, timeout: 300}
  - {class: ExportDeviceEnvironment, description: Exports environment variables to
      the device, level: '2.5', max_retries: 1, name: export-device-env, summary: Exports
      environment variables action, timeout: 300}
  summary: lxc boot
  timeout: 300
- class: DownloadAction
  description: download files and copy to LXC if available
  level: '3'
  max_retries: 1
  name: download-deploy
  pipeline:
  - {class: ConnectDevice, description: use the configured command to connect serial
      to the device, level: '3.1', max_retries: 1, name: connect-device, summary: run
      connection command, timeout: 2400}
  - class: ResetDevice
    description: reboot or power-cycle the device
    level: '3.2'
    max_retries: 1
    name: reset-device
    pipeline:
    - {class: PDUReboot, description: issue commands to a PDU to power cycle a device,
      level: 3.2.1, max_retries: 1, name: pdu-reboot, summary: hard reboot using PDU,
      timeout: 2400}
    summary: reboot the device
    timeout: 2400
  - class: DownloaderAction
    description: download with retry
    level: '3.3'
    max_retries: 3
    name: download-retry
    pipeline:
    - {class: HttpDownloadAction, description: use http to download the file, level: 3.3.1,
      max_retries: 1, name: http-download, summary: http download, timeout: 2400,
      url: 'https://snapshots.linaro.org/member-builds/qcomlt/linux-integration/qcs404-evb-1000/250/boot-linux-integration-v5.0-rc7-233-g01d2757b1757-250-qcs404-evb-1000.img'}
    summary: download-retry
    timeout: 2400
  - class: OverlayAction
    description: add lava scripts during deployment for test shell use
    level: '3.4'
    max_retries: 1
    name: lava-overlay
    pipeline:
    - {class: SshAuthorize, description: include public key in overlay and authorize
        root user, level: 3.4.1, max_retries: 1, name: ssh-authorize, summary: add
        public key to authorized_keys, timeout: 2400}
    - {class: VlandOverlayAction, description: Populate specific vland scripts for
        tests to lookup vlan data., level: 3.4.2, max_retries: 1, name: lava-vland-overlay,
      summary: Add files detailing vlan configuration., timeout: 2400}
    - {class: MultinodeOverlayAction, description: add lava scripts during deployment
        for multinode test shell use, level: 3.4.3, max_retries: 1, name: lava-multinode-overlay,
      summary: overlay the lava multinode scripts, timeout: 2400}
    - class: TestDefinitionAction
      description: load test definitions into image
      level: 3.4.4
      max_retries: 1
      name: test-definition
      pipeline:
      - {class: InlineRepoAction, description: apply inline test definition to the
          test image, level: 3.4.4.1, max_retries: 1, name: inline-repo-action, summary: extract
          inline test definition, timeout: 2400}
      - {class: TestOverlayAction, description: overlay test support files onto image,
        level: 3.4.4.2, max_retries: 1, name: test-overlay, summary: applying LAVA
          test overlay, timeout: 2400}
      - {class: TestInstallAction, description: overlay dependency installation support
          files onto image, level: 3.4.4.3, max_retries: 1, name: test-install-overlay,
        summary: applying LAVA test install scripts, timeout: 2400}
      - {class: TestRunnerAction, description: overlay run script onto image, level: 3.4.4.4,
        max_retries: 1, name: test-runscript-overlay, summary: applying LAVA test
          run script, timeout: 2400}
      summary: loading test definitions
      timeout: 2400
    - {class: CompressOverlay, description: Create a lava overlay tarball and store
        alongside the job, level: 3.4.5, max_retries: 1, name: compress-overlay, summary: Compress
        the lava overlay files, timeout: 2400}
    - {class: PersistentNFSOverlay, description: unpack overlay into persistent NFS,
      level: 3.4.6, max_retries: 1, name: persistent-nfs-overlay, summary: add test
        overlay to NFS, timeout: 2400}
    summary: overlay the lava support scripts
    timeout: 2400
  - {class: CopyToLxcAction, description: copy files to lxc, level: '3.5', max_retries: 1,
    name: copy-to-lxc, summary: copy to lxc, timeout: 2400}
  summary: download deployment
  timeout: 2400
- class: TestShellRetry
  description: Retry wrapper for lava-test-shell
  level: '4'
  max_retries: 1
  name: lava-test-retry
  pipeline:
  - {class: TestShellAction, description: Executing lava-test-runner, level: '4.1',
    max_retries: 1, name: lava-test-shell, summary: Lava Test Shell, timeout: 1800}
  summary: Retry support for Lava Test Shell
  timeout: 1800
- class: FastbootAction
  description: download files and deploy using fastboot
  level: '5'
  max_retries: 1
  name: fastboot-deploy
  pipeline:
  - class: OverlayAction
    description: add lava scripts during deployment for test shell use
    level: '5.1'
    max_retries: 1
    name: lava-overlay
    pipeline:
    - {class: SshAuthorize, description: include public key in overlay and authorize
        root user, level: 5.1.1, max_retries: 1, name: ssh-authorize, summary: add
        public key to authorized_keys, timeout: 2400}
    - {class: VlandOverlayAction, description: Populate specific vland scripts for
        tests to lookup vlan data., level: 5.1.2, max_retries: 1, name: lava-vland-overlay,
      summary: Add files detailing vlan configuration., timeout: 2400}
    - {class: MultinodeOverlayAction, description: add lava scripts during deployment
        for multinode test shell use, level: 5.1.3, max_retries: 1, name: lava-multinode-overlay,
      summary: overlay the lava multinode scripts, timeout: 2400}
    - class: TestDefinitionAction
      description: load test definitions into image
      level: 5.1.4
      max_retries: 1
      name: test-definition
      pipeline:
      - {class: InlineRepoAction, description: apply inline test definition to the
          test image, level: 5.1.4.1, max_retries: 1, name: inline-repo-action, summary: extract
          inline test definition, timeout: 2400}
      - {class: TestOverlayAction, description: overlay test support files onto image,
        level: 5.1.4.2, max_retries: 1, name: test-overlay, summary: applying LAVA
          test overlay, timeout: 2400}
      - {class: TestInstallAction, description: overlay dependency installation support
          files onto image, level: 5.1.4.3, max_retries: 1, name: test-install-overlay,
        summary: applying LAVA test install scripts, timeout: 2400}
      - {class: TestRunnerAction, description: overlay run script onto image, level: 5.1.4.4,
        max_retries: 1, name: test-runscript-overlay, summary: applying LAVA test
          run script, timeout: 2400}
      summary: loading test definitions
      timeout: 2400
    - {class: CompressOverlay, description: Create a lava overlay tarball and store
        alongside the job, level: 5.1.5, max_retries: 1, name: compress-overlay, summary: Compress
        the lava overlay files, timeout: 2400}
    - {class: PersistentNFSOverlay, description: unpack overlay into persistent NFS,
      level: 5.1.6, max_retries: 1, name: persistent-nfs-overlay, summary: add test
        overlay to NFS, timeout: 2400}
    summary: overlay the lava support scripts
    timeout: 2400
  - {class: ConnectDevice, description: use the configured command to connect serial
      to the device, level: '5.2', max_retries: 1, name: connect-device, summary: run
      connection command, timeout: 2400}
  - class: ResetDevice
    description: reboot or power-cycle the device
    level: '5.3'
    max_retries: 1
    name: reset-device
    pipeline:
    - {class: PDUReboot, description: issue commands to a PDU to power cycle a device,
      level: 5.3.1, max_retries: 1, name: pdu-reboot, summary: hard reboot using PDU,
      timeout: 2400}
    summary: reboot the device
    timeout: 2400
  - class: DownloaderAction
    description: download with retry
    level: '5.4'
    max_retries: 3
    name: download-retry
    pipeline:
    - {class: LxcDownloadAction, description: Map to the correct lxc path, level: 5.4.1,
      max_retries: 1, name: lxc-download, summary: lxc download, timeout: 2400, url: 'lxc:/boot-linux-integration-v5.0-rc7-233-g01d2757b1757-250-qcs404-evb-1000.img'}
    summary: download-retry
    timeout: 2400
  - {class: DeployDeviceEnvironment, description: deploy device environment, level: '5.5',
    max_retries: 1, name: deploy-device-env, summary: deploy device environment, timeout: 2400}
  - class: FastbootFlashOrderAction
    description: Determine support for each flash operation
    level: '5.6'
    max_retries: 1
    name: fastboot-flash-order-action
    pipeline:
    - {class: ReadFeedback, description: Check for messages on all other namespaces,
      level: 5.6.1, max_retries: 1, name: read-feedback, summary: Read from other
        namespaces, timeout: 2400}
    - {class: WaitDeviceBoardID, description: wait for udev device with board ID,
      level: 5.6.2, max_retries: 1, name: wait-device-boardid, summary: wait for udev
        device with board ID, timeout: 2400}
    - {class: FastbootFlashAction, description: Run a specified flash command, level: 5.6.3,
      max_retries: 1, name: fastboot-flash-action, summary: Execute fastboot flash
        command, timeout: 2400}
    summary: Handle reset and options for each flash url.
    timeout: 2400
  summary: fastboot deployment
  timeout: 2400
- class: BootFastbootAction
  description: fastboot boot into the system
  level: '6'
  max_retries: 1
  name: fastboot-boot
  pipeline:
  - {class: ConnectDevice, description: use the configured command to connect serial
      to the device, level: '6.1', max_retries: 1, name: connect-device, summary: run
      connection command, timeout: 900}
  - class: ResetDevice
    description: reboot or power-cycle the device
    level: '6.2'
    max_retries: 1
    name: reset-device
    pipeline:
    - {class: PDUReboot, description: issue commands to a PDU to power cycle a device,
      level: 6.2.1, max_retries: 1, name: pdu-reboot, summary: hard reboot using PDU,
      timeout: 900}
    summary: reboot the device
    timeout: 900
  - {class: WaitDeviceBoardID, description: wait for udev device with board ID, level: '6.3',
    max_retries: 1, name: wait-device-boardid, summary: wait for udev device with
      board ID, timeout: 900}
  - {class: FastbootBootAction, description: fastboot boot into system, level: '6.4',
    max_retries: 1, name: boot-fastboot, summary: attempt to fastboot boot, timeout: 900}
  - class: AutoLoginAction
    description: automatically login after boot using job parameters and checking
      for messages.
    level: '6.5'
    max_retries: 1
    name: auto-login-action
    pipeline:
    - {class: LoginAction, description: Real login action., level: 6.5.1, max_retries: 1,
      name: login-action, summary: Login after boot., timeout: 900}
    summary: Auto-login after boot with support for kernel messages.
    timeout: 900
  - {class: ExpectShellSession, description: Wait for a shell, level: '6.6', max_retries: 1,
    name: expect-shell-connection, summary: Expect a shell prompt, timeout: 900}
  - {class: ExportDeviceEnvironment, description: Exports environment variables to
      the device, level: '6.7', max_retries: 1, name: export-device-env, summary: Exports
      environment variables action, timeout: 900}
  summary: fastboot boot
  timeout: 900
- class: TestShellRetry
  description: Retry wrapper for lava-test-shell
  level: '7'
  max_retries: 1
  name: lava-test-retry
  pipeline:
  - {class: TestShellAction, description: Executing lava-test-runner, level: '7.1',
    max_retries: 1, name: lava-test-shell, summary: Lava Test Shell, timeout: 300}
  summary: Retry support for Lava Test Shell
  timeout: 300
- class: FinalizeAction
  description: finish the process and cleanup
  level: '8'
  max_retries: 1
  name: finalize
  pipeline:
  - {class: PowerOff, description: discontinue power to device, level: '8.1', max_retries: 1,
    name: power-off, summary: send power_off command, timeout: 10}
  - {class: ReadFeedback, description: Check for messages on all other namespaces,
    level: '8.2', max_retries: 1, name: read-feedback, summary: Read from other namespaces,
    timeout: 900}
  summary: finalize the job
  timeout: 900
