- class: ScpOverlay
  description: prepare overlay and scp to device
  level: '1'
  max_retries: 1
  name: scp-overlay
  pipeline:
  - class: OverlayAction
    description: add lava scripts during deployment for test shell use
    level: '1.1'
    max_retries: 1
    name: lava-overlay
    pipeline:
    - {class: SshAuthorize, description: include public key in overlay and authorize
        root user, level: 1.1.1, max_retries: 1, name: ssh-authorize, summary: add
        public key to authorized_keys, timeout: 30}
    - {class: VlandOverlayAction, description: Populate specific vland scripts for
        tests to lookup vlan data., level: 1.1.2, max_retries: 1, name: lava-vland-overlay,
      summary: Add files detailing vlan configuration., timeout: 30}
    - {class: MultinodeOverlayAction, description: add lava scripts during deployment
        for multinode test shell use, level: 1.1.3, max_retries: 1, name: lava-multinode-overlay,
      summary: overlay the lava multinode scripts, timeout: 30}
    - class: TestDefinitionAction
      description: load test definitions into image
      level: 1.1.4
      max_retries: 1
      name: test-definition
      pipeline:
      - {class: GitRepoAction, description: apply git repository of tests to the test
          image, level: 1.1.4.1, max_retries: 1, name: git-repo-action, summary: clone
          git test repo, timeout: 30}
      - {class: TestOverlayAction, description: overlay test support files onto image,
        level: 1.1.4.2, max_retries: 1, name: test-overlay, summary: applying LAVA
          test overlay, timeout: 30}
      - {class: TestInstallAction, description: overlay dependency installation support
          files onto image, level: 1.1.4.3, max_retries: 1, name: test-install-overlay,
        summary: applying LAVA test install scripts, timeout: 30}
      - {class: TestRunnerAction, description: overlay run script onto image, level: 1.1.4.4,
        max_retries: 1, name: test-runscript-overlay, summary: applying LAVA test
          run script, timeout: 30}
      - {class: GitRepoAction, description: apply git repository of tests to the test
          image, level: 1.1.4.5, max_retries: 1, name: git-repo-action, summary: clone
          git test repo, timeout: 30}
      - {class: TestOverlayAction, description: overlay test support files onto image,
        level: 1.1.4.6, max_retries: 1, name: test-overlay, summary: applying LAVA
          test overlay, timeout: 30}
      - {class: TestInstallAction, description: overlay dependency installation support
          files onto image, level: 1.1.4.7, max_retries: 1, name: test-install-overlay,
        summary: applying LAVA test install scripts, timeout: 30}
      - {class: TestRunnerAction, description: overlay run script onto image, level: 1.1.4.8,
        max_retries: 1, name: test-runscript-overlay, summary: applying LAVA test
          run script, timeout: 30}
      - {class: InlineRepoAction, description: apply inline test definition to the
          test image, level: 1.1.4.9, max_retries: 1, name: inline-repo-action, summary: extract
          inline test definition, timeout: 30}
      - {class: TestOverlayAction, description: overlay test support files onto image,
        level: 1.1.4.10, max_retries: 1, name: test-overlay, summary: applying LAVA
          test overlay, timeout: 30}
      - {class: TestInstallAction, description: overlay dependency installation support
          files onto image, level: 1.1.4.11, max_retries: 1, name: test-install-overlay,
        summary: applying LAVA test install scripts, timeout: 30}
      - {class: TestRunnerAction, description: overlay run script onto image, level: 1.1.4.12,
        max_retries: 1, name: test-runscript-overlay, summary: applying LAVA test
          run script, timeout: 30}
      summary: loading test definitions
      timeout: 30
    - {class: CompressOverlay, description: Create a lava overlay tarball and store
        alongside the job, level: 1.1.5, max_retries: 1, name: compress-overlay, summary: Compress
        the lava overlay files, timeout: 30}
    - {class: PersistentNFSOverlay, description: unpack overlay into persistent NFS,
      level: 1.1.6, max_retries: 1, name: persistent-nfs-overlay, summary: add test
        overlay to NFS, timeout: 30}
    summary: overlay the lava support scripts
    timeout: 30
  - class: PrepareOverlayScp
    description: copy the overlay over an existing ssh connection
    level: '1.2'
    max_retries: 1
    name: prepare-scp-overlay
    pipeline:
    - {class: ExtractRootfs, description: unpack rootfs, level: 1.2.1, max_retries: 1,
      name: extract-rootfs, summary: 'unpack rootfs, ready to apply lava overlay',
      timeout: 30}
    - {class: ExtractModules, description: extract supplied kernel modules, level: 1.2.2,
      max_retries: 1, name: extract-modules, summary: extract kernel modules, timeout: 30}
    summary: scp the overlay to the remote device
    timeout: 30
  - {class: DeployDeviceEnvironment, description: deploy device environment, level: '1.3',
    max_retries: 1, name: deploy-device-env, summary: deploy device environment, timeout: 30}
  summary: copy overlay to device
  timeout: 30
- class: SshAction
  description: connect over ssh and ensure a shell is found
  level: '2'
  max_retries: 1
  name: login-ssh
  pipeline:
  - {class: Scp, description: copy a file to a known device using scp, level: '2.1',
    max_retries: 1, name: scp-deploy, summary: scp over the ssh connection, timeout: 180}
  - {class: PrepareSsh, description: determine which address to use for primary or
      secondary connections, level: '2.2', max_retries: 1, name: prepare-ssh, summary: set
      the host address of the ssh connection, timeout: 180}
  - {class: ConnectSsh, description: login to a known device using ssh, level: '2.3',
    max_retries: 1, name: ssh-connection, summary: make an ssh connection to a device,
    timeout: 180}
  - class: AutoLoginAction
    description: automatically login after boot using job parameters and checking
      for messages.
    level: '2.4'
    max_retries: 1
    name: auto-login-action
    pipeline:
    - {class: LoginAction, description: Real login action., level: 2.4.1, max_retries: 1,
      name: login-action, summary: Login after boot., timeout: 180}
    summary: Auto-login after boot with support for kernel messages.
    timeout: 180
  - {class: ExpectShellSession, description: Wait for a shell, level: '2.5', max_retries: 1,
    name: expect-shell-connection, summary: Expect a shell prompt, timeout: 180}
  - {class: ExportDeviceEnvironment, description: Exports environment variables to
      the device, level: '2.6', max_retries: 1, name: export-device-env, summary: Exports
      environment variables action, timeout: 180}
  - {class: ScpOverlayUnpack, description: unpack the overlay over an existing ssh
      connection, level: '2.7', max_retries: 1, name: scp-overlay-unpack, summary: unpack
      the overlay on the remote device, timeout: 180}
  summary: login over ssh
  timeout: 180
- {class: MultinodeTestAction, description: Executing lava-test-runner, level: '3',
  max_retries: 1, name: multinode-test, summary: Multinode Lava Test Shell, timeout: 300}
- class: FinalizeAction
  description: finish the process and cleanup
  level: '4'
  max_retries: 1
  name: finalize
  pipeline:
  - {class: PowerOff, description: discontinue power to device, level: '4.1', max_retries: 1,
    name: power-off, summary: send power_off command, timeout: 10}
  - {class: ReadFeedback, description: Check for messages on all other namespaces,
    level: '4.2', max_retries: 1, name: read-feedback, summary: Read from other namespaces,
    timeout: 180}
  summary: finalize the job
  timeout: 180
